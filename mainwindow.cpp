#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "itemmanager.h"
#include "item.h"
#include "scene.h"

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mManager = new ItemManager();
    mScene = new Scene(ui->gvWorkPlace);
    ui->gvWorkPlace->setScene(mScene);
    ui->pbDelete->setEnabled(false);
    connect(ui->pbAddTriangle, SIGNAL(clicked(bool)), this, SLOT(onAddTriangleClick()));
    connect(ui->pbAddSquare, SIGNAL(clicked(bool)), this, SLOT(onAddSquartClick()));
    connect(ui->pbAddCircle, SIGNAL(clicked(bool)), this, SLOT(onAddCircleClick()));
    connect(ui->pbDelete, SIGNAL(clicked(bool)), this, SLOT(onDeleteClick()));
    connect(mManager, SIGNAL(addItem(QAbstractGraphicsShapeItem*)), mScene, SLOT(onAddItem(QAbstractGraphicsShapeItem*)));
    connect(mScene, SIGNAL(clickItem(int)), mManager, SLOT(onChangeFocusItem(int)));
    connect(mScene, SIGNAL(clickItem(int)), this, SLOT(onFocusItem()));
    connect(mManager, SIGNAL(removeItem(QAbstractGraphicsShapeItem*)), mScene, SLOT(onRemoveItem(QAbstractGraphicsShapeItem*)));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mManager;
}

void MainWindow::onAddTriangleClick()
{
    mManager->addNewItem(ItemTypes::ItTriangle);
}

void MainWindow::onAddSquartClick()
{
    mManager->addNewItem(ItemTypes::ItSquare);
}

void MainWindow::onAddCircleClick()
{
    mManager->addNewItem(ItemTypes::ItCircle);
}

void MainWindow::onDeleteClick()
{
    mManager->onRemoveItem();
    ui->pbDelete->setEnabled(false);
}

void MainWindow::onFocusItem()
{
    ui->pbDelete->setEnabled(true);
}

