#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

class ItemManager;
class Scene;

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    ItemManager *mManager;
    Scene *mScene;

public slots:
    void onAddTriangleClick();
    void onAddSquartClick();
    void onAddCircleClick();
    void onDeleteClick();
    void onFocusItem();
};

#endif // MAINWINDOW_H
