#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>

#include <QDebug>
#include <QList>
#include <QDir>
#include <QProcess>
#include <QByteArray>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QDir::setCurrent(qApp->applicationDirPath());

    QTranslator t;
    if( t.load("qtbase_ru.qm") )
    {
        app.installTranslator(&t);
    }
    MainWindow mainWin;
    mainWin.show();

    return app.exec();
}
