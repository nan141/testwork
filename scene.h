#ifndef SCENE_H
#define SCENE_H

#include <QObject>
#include <QGraphicsScene>

class QAbstractGraphicsShapeItem;

/**
 * @brief The Scene class класс графической сцены
 */
class Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit Scene(QObject *parent = nullptr);

signals:
    /**
     * @brief clickItem нажали на объект
     * @param id идентификатор объекта
     */
    void clickItem(int id);

public slots:
    /**
     * @brief onAddItem добавление объекта на сцену
     * @param item объект
     */
    void onAddItem(QAbstractGraphicsShapeItem *item);
    /**
     * @brief onRemoveItem удаление объекта со сцены
     * @param item объект
     */
    void onRemoveItem(QAbstractGraphicsShapeItem *item);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
};

#endif // SCENE_H
