#include "scene.h"
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QTransform>
#include "consts.h"

Scene::Scene(QObject *parent) : QGraphicsScene(parent)
{
    setSceneRect(0, 0, WIDTH, HEIGHT);
}

void Scene::onAddItem(QAbstractGraphicsShapeItem *item)
{
    this->addItem(item);
}

void Scene::onRemoveItem(QAbstractGraphicsShapeItem *item)
{
    this->removeItem(item);
}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (mouseEvent->button() == Qt::LeftButton)
    {
        QGraphicsItem *item = itemAt(mouseEvent->scenePos(), QTransform());
        if(item)
        {
            emit clickItem(item->data(ID).toInt());
        }
    }
}
