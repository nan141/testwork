#include "itemmanager.h"
#include "item.h"
#include <QDebug>
#include "consts.h"

ItemManager::ItemManager(QObject *parent) : QObject(parent)
{}

ItemManager::~ItemManager()
{
    foreach (Item *item, mItems.values())
    {
        delete item;
    }
    mItems.clear();
}

void ItemManager::addNewItem(ItemTypes type)
{
    int height = 50;
    Item *item = nullptr;
    int x = rand() % (WIDTH - height);
    int y = rand() % (HEIGHT - height);
    QColor color = QColor(rand() % 255, rand() % 255, rand() % 255);
    switch (type)
    {
    case ItemTypes::ItCircle:
        item = new Circle(mMaxId++, x, y, height, color);
        break;
    case ItemTypes::ItTriangle:
        item = new Triangle(mMaxId++, x, y, height, height, color);
        break;
    case ItemTypes::ItSquare:
        item = new Square(mMaxId++, x, y, height, color);
        break;
    default:
        qDebug() << QString("Ошибка создания объекта. Неизвестный тип объекта %1").arg(static_cast<int>(type));
        break;
    }
    if(item)
    {
        mItems.insert(item->id(), item);
        emit addItem(item->getViewItem());
    }
}

void ItemManager::onRemoveItem()
{
    if(mFocusItem != -1)
    {
        if(!mItems.contains(mFocusItem))
        {
            return;
        }
        Item *item = mItems.take(mFocusItem);
        emit removeItem(item->getViewItem());
        delete item;
        mFocusItem = -1;
    }
}

void ItemManager::onChangeFocusItem(int id)
{
    if(mFocusItem != -1)
    {
        QMap<int, Item*>::iterator i = mItems.find(mFocusItem);
        if(i != mItems.end())
        {
            i.value()->setFocus(false);
        }
    }
    QMap<int, Item*>::iterator i = mItems.find(id);
    if(i != mItems.end())
    {
        i.value()->setFocus(true);
    }
    mFocusItem = id;
}
