#ifndef ITEM_H
#define ITEM_H
#include<QColor>

class QAbstractGraphicsShapeItem;

/**
 * @brief The Item class класс объектов
 */
class Item
{
public:
    /**
     * @brief Item
     * @param id идентификатор
     * @param x координата X
     * @param y координата Y
     * @param height высота
     * @param width ширина
     * @param color цвет заливки
     */
    Item(int id, int x, int y, int height, int width, QColor color);
    ~Item();
    /**
     * @brief getViewItem возвращает графический объект
     * @return графический объект
     */
    QAbstractGraphicsShapeItem *getViewItem();
    /**
     * @brief id возвращает идентификатор
     * @return идентификатор
     */
    int id();
    /**
     * @brief setFocus установление/сброс фокуса на объекте
     * @param focus Объект в фокусе/ не в фокусе
     */
    void setFocus(bool focus);
protected:
    int mFocusPen;
    int mUnfocusPen;
    int mId;
    int mX;
    int mY;
    int mHeight;
    int mWidth;
    QColor mColor;
    QAbstractGraphicsShapeItem *mView;
    /**
     * @brief setSettingsForView установка настроек для графического объекта
     */
    void setSettingsForView();
};

/**
 * @brief The ItemTypes enum типы объектов
 */
enum class ItemTypes: int
{
    ItCircle,
    ItSquare,
    ItTriangle
};

/**
 * @brief The Circle class объект круг
 */
class Circle: public Item
{
public:
    /**
     * @brief Item
     * @param id идентификатор
     * @param x координата X
     * @param y координата Y
     * @param diameter диаметр
     * @param color цвет заливки
     */
    Circle(int id, int x, int y, int diameter, QColor color);
};

/**
 * @brief The Square class объект квадрат
 */
class Square: public Item
{
public:
    /**
     * @brief Item
     * @param id идентификатор
     * @param x координата X
     * @param y координата Y
     * @param height высота
     * @param color цвет заливки
     */
    Square(int id, int x, int y, int height, QColor color);
};

/**
 * @brief The Triangle class объект треугольник
 */
class Triangle: public Item
{
public:
    /**
     * @brief Item
     * @param id идентификатор
     * @param x координата X
     * @param y координата Y
     * @param height высота
     * @param width ширина
     * @param color цвет заливки
     */
    Triangle(int id, int x, int y, int height, int width, QColor color);
};

#endif // ITEM_H
