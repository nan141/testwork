#include "item.h"

#include <QColor>
#include <QBrush>
#include <QPen>
#include <QGraphicsEllipseItem>
#include <QGraphicsRectItem>
#include <QGraphicsPathItem>
#include <QAbstractGraphicsShapeItem>
#include <QPainterPath>
#include "consts.h"

Item::Item(int id, int x, int y, int height, int width, QColor color) :
    mFocusPen(4), mUnfocusPen(2), mId(id), mX(x), mY(y), mHeight(height), mWidth(width), mColor(color)
{}

Item::~Item()
{
    delete mView;
}

QAbstractGraphicsShapeItem *Item::getViewItem()
{
    return mView;
}

int Item::id()
{
    return mId;
}

void Item::setFocus(bool focus)
{
    mView->setPen(QPen(QBrush(QColor(Qt::black)), focus ? mFocusPen : mUnfocusPen));
}

void Item::setSettingsForView()
{
    mView->setData(ID, mId);
    mView->setFlag(QGraphicsItem::ItemIsFocusable, true);
    mView->setAcceptTouchEvents(true);
}

Circle::Circle(int id, int x, int y, int diameter, QColor color) :
    Item(id, x, y, diameter, diameter, color)
{
    mView = new QGraphicsEllipseItem(mX, mY, mWidth, mHeight);
    mView->setPen(QPen(QBrush(QColor(Qt::black)), mUnfocusPen));
    mView->setBrush(QBrush(mColor));
    setSettingsForView();
}

Square::Square(int id, int x, int y, int height, QColor color) :
    Item(id, x, y, height, height, color)
{
    mView = new QGraphicsRectItem(mX, mY, mWidth, mHeight);
    mView->setPen(QPen(QBrush(QColor(Qt::black)), mUnfocusPen));
    mView->setBrush(QBrush(mColor));
    setSettingsForView();
}

Triangle::Triangle(int id, int x, int y, int height, int width, QColor color) :
    Item(id, x, y, height, width, color)
{
    QPainterPath path = QPainterPath();
    path.moveTo(0, mHeight);
    path.lineTo(mWidth / 2, 0);
    path.lineTo(mWidth, mHeight);
    path.lineTo(0, mHeight);
    mView = new QGraphicsPathItem(path);
    mView->setPos(mX, mY);
    mView->setPen(QPen(QBrush(QColor(Qt::black)), mUnfocusPen));
    mView->setBrush(QBrush(mColor));
    setSettingsForView();
}

