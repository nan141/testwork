#ifndef ITEMMANAGER_H
#define ITEMMANAGER_H

#include <QObject>
#include <QMap>

class Item;
enum class ItemTypes;
class QAbstractGraphicsShapeItem;

/**
 * @brief The ItemManager class менеджер объектов
 */
class ItemManager : public QObject
{
    Q_OBJECT
private:
    /**
     * @brief mItems объекты
     */
    QMap<int, Item*> mItems;
    /**
     * @brief mFocusItem идентификатор объекта под фокусом
     */
    int mFocusItem = -1;
    /**
     * @brief mMaxId максимальный идентификатор
     */
    int mMaxId = 0;

public:
    explicit ItemManager(QObject *parent = nullptr);
    ~ItemManager();
    /**
     * @brief addNewItem добавление нового объекта
     * @param type тип объекта
     */
    void addNewItem(ItemTypes type);
    /**
     * @brief onRemoveItem удаление объекта под фокусом
     */
    void onRemoveItem();

signals:
    /**
     * @brief removeItem удалить графический объект
     * @param view графический объект
     */
    void removeItem(QAbstractGraphicsShapeItem *view);
    /**
     * @brief addItem добавить графический объект
     * @param view графический объект
     */
    void addItem(QAbstractGraphicsShapeItem *view);

public slots:
    /**
     * @brief onChangeFocusItem изменить объект под фокусом
     * @param id идентификатор нового объекта под фокусом
     */
    void onChangeFocusItem(int id);
};

#endif // ITEMMANAGER_H
