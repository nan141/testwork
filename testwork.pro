TARGET = testwork
TEMPLATE = app
DESTDIR = ./bin
OBJECTS_DIR = ./obj
MOC_DIR = ./obj
UI_DIR = ./obj
RCC_DIR = ./obj

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    item.cpp \
    itemmanager.cpp \
    scene.cpp

FORMS += \
    mainwindow.ui

HEADERS += \
    consts.h \
    mainwindow.h \
    item.h \
    itemmanager.h \
    scene.h
